import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { RepoInfo } from '../models/repoInfo';
import { BranchInfo } from '../models/branchinfo';

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  public repos: BehaviorSubject<RepoInfo[]>;

  constructor(private http: HttpClient) {
    this.repos = new BehaviorSubject<RepoInfo[]>([]);
  }

  getUserRepos(userName: String){
    const url = `https://api.github.com/users/${userName}/repos`;

    return this.http.get<any[]>(url).pipe(map(data => { 
      const result = new Array<RepoInfo>();

      data.forEach(element => {
        const repo = new RepoInfo();

        repo.name = element.name;
        repo.ownerLogin = element.owner.login;

        result.push(repo);
      });

      this.repos.next(result);
      return result;
    }));;
  }

  getRepoBranches(selectedUser, selectedRepo){
    const url = `https://api.github.com/repos/${selectedUser}/${selectedRepo}/branches`;

    return this.http.get<any>(url).pipe(map(data => {
      const result = new Array<BranchInfo>();

      data.forEach(element => {
        const branch = new BranchInfo();

        branch.name = element.name;
        branch.sha = element.commit.sha;

        result.push(branch);
      });

      return result;
     }));      
  }
}
