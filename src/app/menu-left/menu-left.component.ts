import { Component, OnInit } from '@angular/core';
import { GithubService } from '../_services/github.service';
import { UserSwitchComponent } from '../user-switch/user-switch.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit {

  constructor(
    private githubService: GithubService,
    public dialog: MatDialog) { }

  ngOnInit() {
  }

  onUserChangeClick() {
    const dialogRef = this.dialog.open(UserSwitchComponent, {
      width: '500px',
      height: '300px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
           this.githubService.getUserRepos(result).subscribe();
        }
    });
  }

}
