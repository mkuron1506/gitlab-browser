import { Component, OnInit } from '@angular/core';
import { GithubService } from '../_services/github.service';
import { RepoInfo } from '../models/repoInfo';
import { MatDialog } from '@angular/material/dialog';
import { BranchListComponent } from '../branch-list/branch-list.component';

@Component({
  selector: 'app-repo-list',
  templateUrl: './repo-list.component.html',
  styleUrls: ['./repo-list.component.scss'],
})
export class RepoListComponent implements OnInit {
  dataSource:  RepoInfo[];

  columnsToDisplay = ['name', 'ownerLogin'];

  constructor(
    private githubService: GithubService,
    public dialog: MatDialog) 
    {
        console.log("RepoListComponent");
    }

  ngOnInit() {
    this.githubService.repos.subscribe((r) => 
    { 
       this.dataSource = r;
    });
  }

  onRowClick(row){
    const dialogRef = this.dialog.open(BranchListComponent, {
      width: '1000px',
      height: 'auto',
      data: { selectedUser: row.ownerLogin, selectedRepo: row.name }
    });
  }
}

