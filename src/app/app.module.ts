import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatCardModule} from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule} from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http'; 
import { MatTableModule} from '@angular/material/table';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { RepoListComponent } from './repo-list/repo-list.component';
import { HeaderComponent } from './header/header.component';
import { MenuLeftComponent } from './menu-left/menu-left.component';
import { UserSwitchComponent } from './user-switch/user-switch.component';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BranchListComponent } from './branch-list/branch-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RepoListComponent,
    HeaderComponent,
    MenuLeftComponent,
    UserSwitchComponent,
    BranchListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatDialogModule
  ],
  entryComponents: [
    UserSwitchComponent,
    BranchListComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
