import { Component, OnInit, Inject } from '@angular/core';
import { GithubService } from '../_services/github.service';
import { BranchInfo } from '../models/branchinfo';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.scss']
})
export class BranchListComponent implements OnInit {
  dataSource:  BranchInfo[] = [];
  columnsToDisplay = ['name', 'sha'];

  constructor(
    private githubService: GithubService,
    public dialogRef: MatDialogRef<BranchListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.githubService.getRepoBranches(this.data.selectedUser, this.data.selectedRepo).subscribe(
    (res)=> {
      this.dataSource = res;
    });
  }

  onCloseClick(){
    this.dialogRef.close();
  }
}
