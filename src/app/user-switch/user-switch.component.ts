import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-agent-switch',
  templateUrl: './user-switch.component.html'
})
export class UserSwitchComponent implements OnInit {
  name: string;

  constructor(public dialogRef: MatDialogRef<UserSwitchComponent>) { }

  ngOnInit(): void {
  }

  onConfirmClick() {
    this.dialogRef.close(this.name);
  }

  onCancelClick() {
    this.dialogRef.close();
  }
}
